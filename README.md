# A-Core Execution Environment Interface

This repository documents the Execution Environment Interface (EEI) of A-Core.

## Background
The RISC-V [unprivileged specification] views a RISC-V hardware platform as an abstract resource capable of executing instructions within some execution environment. Many important details affecting program behavior, such as memory mapping and power-on state are left unconstrained by the specification. Instead, they are left to be defined by the EEI. However, since program behavior depends on the EEI, it should be explicitly defined with clear documentation.

## Directory Structure
`a-core_eei.md` -- A-Core EEI specification.

[unprivileged specification]: https://github.com/riscv/riscv-isa-manual/releases/download/Ratified-IMAFDQC/riscv-spec-20191213.pdf