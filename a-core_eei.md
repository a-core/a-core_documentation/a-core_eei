# TODO
* [ ] Exceptions, Interrupts and Traps
* [ ] How is execution terminated and reported to the external environment (fatal trap)
* [ ] Program execution environment
  * [x] setup stack, global variables, etc. before calling C main function
  * [x] How does the execution environment map to hardware (e.g. what address range is RAM mapped to? Where is the stack located?)


# A-Core RISC-V Execution Environment Interface
The Execution Environment Interface (EEI) dictates how software running on a RISC-V hardware platform will behave. Many important details, such as memory mapping and power-on state are not defined by the RISC-V unprivileged ISA specification. The specification views a RISC-V hardware platform as an abstract resource capable of executing instructions. However, since the behaviour of the software depends on the EEI, it should be made explicit with clear documentation.

## Memory Map
The RISC-V specification defines three types for memory areas:
* Vacant
* Main memory
* I/O memory
Attempting to address vacant memory should generate an exception. Addressing main memory should have no side effects. Addressing I/O memory can have side effects.

The memory is little-endian, i.e. the lowest order byte of a multi-byte value is stored in the lowest memory address.

| Purpose        | Start Address (inclusive) | End Address (inclusive) | Type        | Access |
| -------------- | ------------------------- | ----------------------- | ----------  | ------ |
| Sim. STDOUT    | 0x3000_1000               | 0x3000_1003             | I/O memory  | W      |
| GPIO           | 0x3000_0000               | 0x3000_001F             | I/O memory  | RW     |
| RAM            | 0x2000_0000               | 0x2000_3FFF             | Main memory | RW     |
| Trap Handler   | 0x0001_F000               | 0x0001_FFFF             | Main memory | R      |
| ROM            | 0x0001_0000               | 0x0001_0FFF             | Main memory | R      |

## Control and Status Regsiters (CSRs)

### Machine-Level CSRs
| Number | Privilege | Name | Description |
| ------ | --------- | ---- | ----------- |
| `0xB00` | MRW | `mcycle` | Machine cycle counter. |
| `0xB02` | MRW | `minstret` | Machine instructions-retired counter. |
| `0xB80` | MRW | `mcycleh` | Upper 32 bits of `mcycle`. |
| `0xB82` | MRW | `minstreth` | Upper 32 bits of `minstret`. |
|

### Nonstandard CSRs
| Number | Privilege | Name | Description |
| ------ | --------- | ---- | ----------- |
| `0xBC0` | MRW | `mstopsim` | Stop simulation control. |

* Writing any nonzero value to `mstopsim` signals the executing simulator to exit the ongoing simulation.
